<?php

/**
 * Implement hook_form_alter().
 *
 * Allows the profile to alter the site-configuration form. This is
 * called through custom invocation, so $form_state is not populated.
 */
function northdrop_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure') {
    // Set default for site name field.
    $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  }
}

function northdrop_install_tasks($install_state) {
  $tasks = array();
  $tasks = array(
    // This is an example of a task that defines a form which the user who is
    // installing the site will be asked to fill out. To implement this task,
    // your profile would define a function named myprofile_data_import_form()
    // as a normal form API callback function, with associated validation and
    // submit handlers. In the submit handler, in addition to saving whatever
    // other data you have collected from the user, you might also call
    // variable_set('myprofile_needs_batch_processing', TRUE) if the user has
    // entered data which requires that batch processing will need to occur
    // later on.
    'northdrop_data_generation_form' => array(
      'display_name' => st('Site Size'),
      'type' => 'form',
    ),
  );
  
  return $tasks;
}

function northdrop_data_generation_form($form, &$form_state) {
  
  $sample_sizes = _northdrop_get_sample_sizes();
  $options = array();
  foreach ($sample_sizes as $key => $spec) {
    $options[$key] = ucfirst($key);
  }

  $options['custom'] = 'Custom';

  $default_size = key(array_slice($sample_sizes, 0, 1));
  

  $form['size'] = array(
    '#type' => 'radios',
    '#title' => 'Site size',
    '#options' => $options,
    '#default_value' => $default_size,
  );

  $form['advanced'] = array (
    '#type' => 'fieldset',
    '#title' => 'Advanced',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $fields = array(
    'nodes' => 'Nodes',
    'comments' => 'Comments per node',
    'users' => 'Users',
    'terms' => 'Categories',
    'vocabs' => 'Category Types',
  );

  foreach ($fields as $key => $title) {
    $form['advanced'][$key] = array (
      '#title' => $title,
      '#type' => 'textfield',
      '#default_value' => $sample_sizes[$default_size][$key],
    );
  }

  $form['submit'] = array (
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

function northdrop_data_generation_form_submit($form, &$form_state) {
  $size_choosen = $form_state['values']['size'];
  if ($size_choosen == 'custom') {
    $generate = $form_state['values'];
  }
  else {
    $sample_sizes = _northdrop_get_sample_sizes();
    $generate = $sample_sizes[$size_choosen];
  }
  northdrop_setup_users();
  northdrop_data_generate($generate);
  
}

function northdrop_setup_users() {
  $editor = new StdClass();
  $editor->name = 'editor';
  user_role_save($editor);
  $editor = user_role_load('editor');
  
  $perms = array('administer nodes');
  user_role_grant_permissions($editor->rid, $perms);
  $edit = array(
        'name'    => 'editor',
        'pass'    => 'editor',
        'mail'    => 'editor' . '@' . 'example.com',
        'status'  => 1,
        'created' => REQUEST_TIME,
        'roles'   => array($editor->rid => 'Is this an API?  Why form_keys? Because it is in a form? DAMN IT DRUPAL!', DRUPAL_AUTHENTICATED_RID => 'Seriously.'),
      );
  $account = user_save(NULL, $edit);

  drupal_set_message('Created user "editor" with password "editor"');
  
}

function northdrop_data_generate($generate) {
  
  module_load_include('inc', 'devel_generate');
  devel_create_users($generate['users'], FALSE);
  devel_generate_taxonomy_data($generate['vocabs'], $generate['terms'], 12, FALSE);

  // Generate nodes.
  // This is kinda hacky because there isn't *really* an API for this.
  $results = array();
  $results['users'] = NULL;

  $results['node_types'] = array ('article' => 'article', 'page' => 'page');
  $results['title_length'] = 4;
  //$results['add_upload'] = TRUE;
  $results['add_terms'] = TRUE;
  $results['num_comments'] = $generate['comments'];
  $results['add_alias'] = TRUE;
  $results['add_statistics'] = TRUE;

  devel_generate_content_pre_node($results);
  for ($i = 1; $i <= $generate['nodes']; $i++) {
    devel_generate_content_add_node($results);
  }
}

function _northdrop_get_sample_sizes() {
  $sample_sizes = array();

  $sample_sizes['small'] = array();
  $sample_sizes['small']['nodes'] = 200;
  $sample_sizes['small']['comments'] = 4;
  $sample_sizes['small']['users'] = 50;
  $sample_sizes['small']['terms'] = 10;
  $sample_sizes['small']['vocabs'] = 2;

  $sample_sizes['medium'] = array();
  $sample_sizes['medium']['nodes'] = 3000;
  $sample_sizes['medium']['comments'] = 15;
  $sample_sizes['medium']['users'] = 1000;
  $sample_sizes['medium']['terms'] = 100;
  $sample_sizes['medium']['vocabs'] = 5;

  $sample_sizes['large'] = array();
  $sample_sizes['large']['nodes'] = 10000;
  $sample_sizes['large']['comments'] = 15;
  $sample_sizes['large']['users'] = 2500;
  $sample_sizes['large']['terms'] = 300;
  $sample_sizes['large']['vocabs'] = 5;

  return $sample_sizes;
}